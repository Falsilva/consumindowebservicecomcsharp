﻿namespace ConsumindoWebserviceComCSharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConsumirAPI = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnConsumirAPI
            // 
            this.btnConsumirAPI.Location = new System.Drawing.Point(50, 43);
            this.btnConsumirAPI.Name = "btnConsumirAPI";
            this.btnConsumirAPI.Size = new System.Drawing.Size(135, 23);
            this.btnConsumirAPI.TabIndex = 0;
            this.btnConsumirAPI.Text = "Consumir API";
            this.btnConsumirAPI.UseVisualStyleBackColor = true;
            this.btnConsumirAPI.Click += new System.EventHandler(this.btnConsumirAPI_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(241, 106);
            this.Controls.Add(this.btnConsumirAPI);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Web Service Client";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnConsumirAPI;
    }
}

