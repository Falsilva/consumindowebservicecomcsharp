﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsumindoWebserviceComCSharp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnConsumirAPI_Click(object sender, EventArgs e)
        {
            string objetoStringParaEnvio = GetObjetoParaEnvio();
            string retorno = FazRequestSWAPI(objetoStringParaEnvio);

            MessageBox.Show(retorno, "Resultado", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

		private string FazRequestSWAPI(string objetoStringParaEnvio)
		{
			string response = "Não fui feliz!";

			string url = "https://swapi.dev/api/people/3";

			Uri uri = new Uri(url);

			// Cria o objeto que faz a requisição
			WebRequest webRequest = WebRequest.Create(uri);
			HttpWebRequest httpWR = (HttpWebRequest)webRequest;

			// Tempo do ciclo de vida entre para obter uma resposta, sendo o total da espera da requisição do GetRequestStream e do GetResponse juntos
			// O timeout 3000, não é suficiente, pois depende do tempo de leitura e escrita dos dados recebidos, portanto deve-se ajustar os dois tempos
			httpWR.Timeout = 9000;

			// Tempo para ler ou escrever dados depois da conexão estabelecida, deve ser menor que o timeout.
			// Tempo utilizado no GetRequestStream e também no GetResponse
			// Deve setar esse timeout, pois o seu valor padrão é 5 minutos = 300000 milissegundos
			httpWR.ReadWriteTimeout = 2000;

			// Indica o tamanho do corpo da entidade, em bytes, enviado ao destinatário
			//httpWR.ContentLength = Encoding.UTF8.GetBytes(objetoStringParaEnvio).Length;
			//httpWR.ContentType = "application/soap+xml;charset=\"UTF-8\"";
			httpWR.Method = "GET";

			try
			{
				// Faz a requisição para obter um stream de escrita de dados na requisição e em seguida, faz a escrita dos dados na requisição
				//using (Stream reqStream = httpWR.GetRequestStream())
				//using (StreamWriter streamWriter = new StreamWriter(reqStream))
				//{
				//	// Grava o XML na requisição, sendo os parâmetros, o buffer (xml), o index (posição do caractere no buffer, o qual inicia a leitura) e o count (o número máximo de caracteres a serem gravados)
				//	streamWriter.Write(objetoStringParaEnvio, 0, Encoding.UTF8.GetBytes(objetoStringParaEnvio).Length);
				//}

				// Faz a requisição com GetResponse, retornando um WebResponse
				WebResponse webResponse = httpWR.GetResponse();

				// Faz a leitura do WebResponse, armazenando numa string
				using (Stream respStream = webResponse.GetResponseStream())
				{
					StreamReader streamReader = new StreamReader(respStream);
					response = streamReader.ReadToEnd();
				}
			}
			catch (Exception ex)
			{
				return $"Xiiii!! Deu ruim!! Erro: {ex.Message}";
			}

			return response;
		}




		private string FazRequestParaAPI(string objetoStringParaEnvio)
        {
            string response = "Não fui feliz!";


//			XmlDocument inputDocument = new XmlDocument();

//			inputDocument.LoadXml(xDocInput.OuterXml);
//			_inputMessage = inputDocument;

//			string xmlRetorno = string.Empty;

//			string xmlSoap = string.Format(
//								@"<?xml version=""1.0"" encoding=""utf-8""?>
//<soapenv:Envelope xmlns:soapenv = ""http://schemas.xmlsoap.org/soap/envelope"">
//<soapenv:Header/>
//<soapenv:Body>{0}</soapenv:Body>
//</soapenv:Envelope>",
//								  xDocInput.OuterXml);



			string url = "";

			Uri uri = new Uri(url);

			// Cria o objeto que faz a requisição
			WebRequest webRequest = WebRequest.Create(uri);
			HttpWebRequest httpWR = (HttpWebRequest)webRequest;

			// Tempo do ciclo de vida entre para obter uma resposta, sendo o total da espera da requisição do GetRequestStream e do GetResponse juntos
			// O timeout 3000, não é suficiente, pois depende do tempo de leitura e escrita dos dados recebidos, portanto deve-se ajustar os dois tempos
			httpWR.Timeout = 9000;

			// Tempo para ler ou escrever dados depois da conexão estabelecida, deve ser menor que o timeout.
			// Tempo utilizado no GetRequestStream e também no GetResponse
			// Deve setar esse timeout, pois o seu valor padrão é 5 minutos = 300000 milissegundos
			httpWR.ReadWriteTimeout = 2000;

			// Indica o tamanho do corpo da entidade, em bytes, enviado ao destinatário
			httpWR.ContentLength = Encoding.UTF8.GetBytes(objetoStringParaEnvio).Length;
			httpWR.ContentType = "application/soap+xml;charset=\"UTF-8\"";
			httpWR.Method = "POST";

			try
			{
				// Faz a requisição para obter um stream de escrita de dados na requisição e em seguida, faz a escrita dos dados na requisição
				using (Stream reqStream = httpWR.GetRequestStream())
				using (StreamWriter streamWriter = new StreamWriter(reqStream))
				{
					// Grava o XML na requisição, sendo os parâmetros, o buffer (xml), o index (posição do caractere no buffer, o qual inicia a leitura) e o count (o número máximo de caracteres a serem gravados)
					streamWriter.Write(objetoStringParaEnvio, 0, Encoding.UTF8.GetBytes(objetoStringParaEnvio).Length);
				}

				// Faz a requisição com GetResponse, retornando um WebResponse
				WebResponse webResponse = httpWR.GetResponse();

				// Faz a leitura do WebResponse, armazenando numa string
				using (Stream respStream = webResponse.GetResponseStream())
				{
					StreamReader streamReader = new StreamReader(respStream);
					response = streamReader.ReadToEnd();
				}
			}
			catch (Exception ex)
			{
				return $"Xiiii!! Deu ruim!! Erro: {ex.Message}";
			}

			// Carrega a string lida em um objeto XML
			//XmlDocument xmlret = new XmlDocument();
			//xmlret.LoadXml(xmlRetorno);

			//XmlNamespaceManager ns = new XmlNamespaceManager(xmlret.NameTable);
			//ns.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
			//ns.AddNamespace("SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/");

			//return xmlret.DocumentElement.SelectSingleNode(@"SOAP-ENV:Body", ns).InnerXml;
			
			return response;
        }

        private string GetObjetoParaEnvio()
        {
            return @"";
        }
    }
}
